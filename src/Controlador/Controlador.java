
package Controlador;

import Modelo.Docentes;
import Vista.dlgDocentes;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener{
    private Docentes doc;
    private dlgDocentes vista;

    public Controlador(Docentes doc, dlgDocentes vista) {
        this.doc = doc;
        this.vista = vista;
        
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.cboNivel.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        
    }
    
    private void iniciarVista(){
        vista.setTitle(":: Docentes ::");
        vista.setSize(796, 606);
        vista.setVisible(true);

    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btnNuevo){
            vista.txtDom.setEnabled(true);
            vista.txtHorasImpartidas.setEnabled(true);
            vista.txtNom.setEnabled(true);
            vista.txtNumDoc.setEnabled(true);
            vista.txtPagoHoraBase.setEnabled(true);
            
            vista.cboNivel.setEnabled(true);
            
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            
        }
        else if(e.getSource() == vista.btnGuardar){
            if(ValiGuard() == true){
                vista.spNivel.setEnabled(true);
                try{
                    doc.setNumDoc(Integer.parseInt(vista.txtNumDoc.getText()));
                    doc.setNom(vista.txtNom.getText());
                    doc.setDom(vista.txtDom.getText());
                    doc.setNivel(String.valueOf(vista.cboNivel.getSelectedIndex()));
                    doc.setPagBase(Float.parseFloat(vista.txtPagoHoraBase.getText()));
                    doc.setHoras(Float.parseFloat(vista.txtHorasImpartidas.getText()));
                    limpiar();
                    
                }
                catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista,"Ocurrio el error: "+ex);
                }
                catch(Exception ex2){
                    JOptionPane.showMessageDialog(vista,"Ocurrio el error: "+ex2);
                }           
            }
            else{
                JOptionPane.showMessageDialog(vista,"No deje ningun espacio en blanco");
            }

            
            
            
        }
        else if(e.getSource() == vista.btnMostrar){
            
            vista.txtPagoHorasImpartidas.setText(String.valueOf(doc.calcularPago()));
            vista.txtPagoBono.setText(String.valueOf(doc.calcularBono(Integer.parseInt(vista.spNivel.getValue().toString()))));
            vista.txtDescImpuesto.setText(String.valueOf(doc.calcularImpuesto()));
            vista.txtTotalPago.setText(String.valueOf(Float.parseFloat(vista.txtPagoHorasImpartidas.getText())+ Float.parseFloat(vista.txtPagoBono.getText())-Float.parseFloat(vista.txtDescImpuesto.getText())));
            
            
            
            
            vista.txtNumDoc.setText(String.valueOf(doc.getNumDoc()));
            vista.txtNom.setText(doc.getNom());
            vista.txtDom.setText(doc.getDom());
            vista.cboNivel.setSelectedIndex(Integer.parseInt(doc.getNivel()));
            vista.txtPagoHoraBase.setText(String.valueOf(doc.getPagBase()));
            vista.txtHorasImpartidas.setText(String.valueOf(doc.getHoras()));
            
        }
        
        else if(e.getSource() == vista.btnLimpiar){
            vista.txtNumDoc.setText(" ");
            vista.txtNom.setText(" ");
            vista.txtDom.setText(" ");
            vista.cboNivel.setSelectedIndex(0);
            vista.txtPagoHoraBase.setText(" ");
            vista.txtHorasImpartidas.setText(" ");
            vista.txtDescImpuesto.setText(" ");
            vista.txtPagoHorasImpartidas.setText(" ");
            vista.txtPagoBono.setText(" ");
            vista.txtDescImpuesto.setText(" ");
            vista.txtTotalPago.setText(" ");
            vista.spNivel.setValue(0);
        }
        else if(e.getSource() == vista.btnCancelar){
            vista.txtDom.setEnabled(false);
            vista.txtHorasImpartidas.setEnabled(false);
            vista.txtNom.setEnabled(false);
            vista.txtNumDoc.setEnabled(false);
            vista.txtPagoHoraBase.setEnabled(false);
            
            vista.cboNivel.setEnabled(false);
            
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.spNivel.setEnabled(false);
            limpiar2();
        }
        else if(e.getSource()==vista.btnCerrar){
            if(JOptionPane.showConfirmDialog(vista,"Seguro que desea cerrar?","Cerrar",JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION){
                
            }
            else{
                vista.setVisible(false);
                vista.dispose();
                System.exit(0);
            }   
        }

    }
    public void limpiar2(){
        vista.txtNumDoc.setText(" ");
        vista.txtNom.setText(" ");
        vista.txtDom.setText(" ");
        vista.cboNivel.setSelectedIndex(0);
        vista.txtPagoHoraBase.setText(" ");
        vista.txtHorasImpartidas.setText(" ");
        vista.txtDescImpuesto.setText(" ");
        vista.txtPagoHorasImpartidas.setText(" ");
        vista.txtPagoBono.setText(" ");
        vista.txtDescImpuesto.setText(" ");
        vista.txtTotalPago.setText(" ");
        vista.spNivel.setValue(0);
    }
    public void limpiar(){
        vista.txtNumDoc.setText(" ");
        vista.txtNom.setText(" ");
        vista.txtDom.setText(" ");
        vista.cboNivel.setSelectedIndex(0);
        vista.txtPagoHoraBase.setText(" ");
        vista.txtHorasImpartidas.setText(" ");
    }
    
    public boolean ValiGuard(){
        if(!vista.txtNumDoc.getText().isEmpty()
                && !vista.txtNom.getText().isEmpty()
                && !vista.txtDom.getText().isEmpty()
                && !vista.txtNumDoc.getText().isEmpty()
                && vista.cboNivel.getSelectedIndex() != 0
                && !vista.txtPagoHoraBase.getText().isEmpty()
                && !vista.txtHorasImpartidas.getText().isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }

    
    public static void main(String[] args) {
        Docentes doc = new Docentes();
        dlgDocentes vista = new dlgDocentes();
        Controlador contr = new Controlador(doc, vista);
        contr.iniciarVista();
    }
    
}
