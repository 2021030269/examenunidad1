
package Modelo;

public class Docentes {
    private int numDoc;
    private String nom,dom,nivel;
    private float pagBase,horas;

    public Docentes() {
        numDoc = 0;
        nom = " ";
        dom = " ";
        nivel = " ";
        pagBase = 0f;
        horas = 0f;
    }

    public Docentes(int numDoc, String nom, String dom, String nivel, float pagBase, float horas) {
        this.numDoc = numDoc;
        this.nom = nom;
        this.dom = dom;
        this.nivel = nivel;
        this.pagBase = pagBase;
        this.horas = horas;
    }
    
    public Docentes(Docentes doc) {
        this.numDoc = doc.numDoc;
        this.nom = doc.nom;
        this.dom = doc.dom;
        this.nivel = doc.nivel;
        this.pagBase = doc.pagBase;
        this.horas = doc.horas;
    }

    public void setNumDoc(int numDoc) {
        this.numDoc = numDoc;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDom(String dom) {
        this.dom = dom;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public void setPagBase(float pagBase) {
        this.pagBase = pagBase;
    }

    public void setHoras(float horas) {
        this.horas = horas;
    }

    public int getNumDoc() {
        return numDoc;
    }

    public String getNom() {
        return nom;
    }

    public String getDom() {
        return dom;
    }

    public String getNivel() {
        return nivel;
    }

    public float getPagBase() {
        return pagBase;
    }

    public float getHoras() {
        return horas;
    }
    
    public float calcularPago(){
        switch(getNivel()){
            case "1":
                return getHoras()*getPagBase()*1.3f;
            case "2":
                return getHoras()*getPagBase()*1.5f;
            case "3":
                return getHoras()*getPagBase()*2;
            
        }
        return 0;
    }
    public float calcularImpuesto(){
        return calcularPago()*16/100;
    }
    public float calcularBono(int cant){
        if(cant == 1 || cant == 2){
            return calcularPago()*5/100;
        }
        else if(cant >= 3 || cant <= 5){
            return calcularPago()*10/100;
        }
        else if(cant > 5){
            return calcularPago()*20/100;
        }
        else{
            return 0;
        }
    }
    
    
}
